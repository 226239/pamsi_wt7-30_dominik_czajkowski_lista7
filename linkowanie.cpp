#include <iostream>
#include <math.h>
#include <exception>
#include "deque.h"


using namespace std;

class THaszujaca;

/* obsluga wyjatkow */
class DontExistException : public std::exception
{
public:
    virtual const char * what() const noexcept
    {
        return "Element o podanym kluczu nie istnieje.";
    }
};

class IsFullException : public std::exception
{
public:
    virtual const char * what() const noexcept
    {
        return "Tablica jest pelna.";
    }
};

//////////////////////////////////////////////////////////

class Element
{
private:
    int wartosc;
    int klucz;
    friend class THaszujaca;

public:
    Element()
    {
        wartosc = 0;
        klucz = 0;
    }
};


class THaszujaca
{
private:
    Deque <Element> *tablica;
    int ilosc_elementow;
    int ilosc_dodanych_elementow;

    bool czy_pierwsza(int); //metoda sprawdzaczy podana liczba jest pierwsza IN: liczba OUT: true - jesli liczba pierwsza; false - jesli liczba nie jest pierwsza
    int hasz(int); //metoda haszujaca zamieniajaca klucze na indeksy tablicy IN: klucz dodanego elementu OUT: wartosc funcki haszujacej

public:
    THaszujaca(int); //konstruktor alokujacy tablice 101 elementow
    ~THaszujaca() {delete [] tablica;} //destruktor dealokujacy tablice

    int get(int); //metoda wyszukuje element o podanym kluczu i zwraca jego wartosc IN: klucz OUT: wartosc
    void put(int,int); //metoda dodaje element o podanym kluczu i wartosc IN: klucz, wartosc
    int remove(int); //metoda usuwa element o podanym kluczu IN: klucz
    int size(); //metoda zwraca ilosc dodanych elementow do tablicy haszujacej OUT: ilosc elementow
    bool isEmpty(); //metoda sprawdza czy tablica haszujaca jest pusta OUT: true - jesli pusta; false - jesli nie jest pusta
};

THaszujaca::THaszujaca(int ile = 101)
{
    if(!czy_pierwsza(ile))
    {
        cerr << "Podana liczna nie jest pierwsza.";
        ile = 101;
    }
    ilosc_elementow = ile;
    tablica = new Deque <Element> [ile];

    ilosc_dodanych_elementow = 0;
}

int THaszujaca::hasz(int klucz)
{
    return (klucz%ilosc_elementow);
}

int THaszujaca::size()
{
    return ilosc_dodanych_elementow;
}

bool THaszujaca::isEmpty()
{
    return !ilosc_dodanych_elementow;
}

int THaszujaca::get(int klucz)
{
    int i = hasz(klucz); //zmienna przechowyje wartosc haszujaca

    cout << "Probki: 1" << endl;
    return tablica[i].front().wartosc;
}

void THaszujaca::put(int klucz,int wart)
{
   if(ilosc_elementow == ilosc_dodanych_elementow) throw IsFullException();
    int i = hasz(klucz); //zmienna przechowyje wartosc haszujaca

    Element wsk;
    wsk.klucz = klucz;
    wsk.wartosc = wart;
    tablica[i].addFront(wsk);
    ilosc_dodanych_elementow++;
    cout << "Probki: 1" << endl;
}


int THaszujaca::remove(int klucz)
{
    int i = hasz(klucz); //zmienna przechowyje wartosc funkcji haszujacej

    int wartosc = tablica[i].front().wartosc;
    tablica[i].removeFront();
    ilosc_dodanych_elementow--;
    cout << "Probki: 1" << endl;

    return wartosc;
}

bool THaszujaca::czy_pierwsza(int liczba)
{
    //przedzal w jakim sprawdzamy dana liczbe
    int prawy = sqrt(liczba);
    int lewy = 2;

    while (lewy <= prawy)
        if(!(liczba%lewy++))
            return false;

    return true;
}
